# Handlungskompetenzen

## Modulspezifische Handlungskompetenzen

 * B15 Nachhaltiges Handeln
   * B15.1 Neue und bereits bestehende ICT-Systeme bezüglich Energieeffizienz und Umweltverträglichkeit evaluieren
   * B15.2 Massnahmen zur Minimierung des Energieverbrauchs ergreifen
   * B15.3 Den Einsatz von Material und natürlichen Ressourcen überwachen
   * B15.4 Massnahmen zum Ersatz und zur Minimierung des Einsatzes von umweltschädigenden Materialien sowie zur Schliessung von Materialkreisläufen ergreifen
   * B15.5 Tätigkeiten an den Kriterien einer ökonomischen, sozialen und ökologischen Nachhaltigkeit sowie ethischer Richtlinien ausrichten
   * B15.6 Interaktionen gegenüber Dritten mit Respekt und Toleranz gestalten
   * B15.7 Arbeitssicherheit und Gesundheitsschutz der Mitarbeitenden sowie Umweltschutz im eigenen Wirkungsbereich als Vorgesetzte/Vorgesetzter verantworten und gestalten

## Allgemeine Handlungskompetenzen


## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
