![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# ECO - Nachhaltigkeit

## Kurzbeschreibung des Moduls 

Dieses Unterrichtsmodul soll den Studierenden ein tieferes Verständnis für die Bedeutung von 
Nachhaltigkeit und Umweltbewusst sein vermitteln und sie dazu befähigen, bewusstere Entscheidungen 
im Einklang mit einer nachhaltigen Zukunft zu treffen.

 - Definition von Nachhaltigkeit und Umweltbewusstsein. 
 - Historische Entwicklung des Nachhaltigkeitskonzepts. 
 - Zusammenhang zwischen menschlichem Handeln und Umweltauswirkungen. 
 - Soziale Gerechtigkeit und Chancengleichheit im Kontext der Nachhaltigkeit. 
 - Fairer Handel und globale Zusammenhänge. 
 - Wirtschaftliche Aspekte der Nachhaltigkeit. 
 - Reduzierung von Abfall und Recycling. 
 - Energieeffizienz und erneuerbare Energien. 
 - Alltagsentscheidungen zur Förderung der Nachhaltigkeit.

## Angaben zum Transfer der erworbenen Kompetenzen 

Vermittlung durch Vorträge, Vertiefung mit Diskussion, Abschluss mit Führung bei einem führenden Schweizer Recyclingunternehmen

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
