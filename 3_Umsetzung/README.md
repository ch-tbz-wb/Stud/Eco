# Umsetzung

 - Bereich: Nachhaltiges Handeln
 - Semester: 2

## Lektionen: 

* Präsenzunterricht: 20
* Fernunterricht: 0
* Selbststudium: 10

## Lernziele
 - Die Bedeutung von Nachhaltigkeit und Umweltbewusstsein verstehen. 
 - Die Auswirkungen menschlichen Handelns auf die Umwelt erkennen. 
 - Handlungsmöglichkeiten zur Förderung der Nachhaltigkeit kennenlernen.

## Voraussetzungen

keine

## Methoden

Vorträge, Diskussionen, Veranschaulichung, Exkursion

## Schlüsselbegriffe

Nachhaltigkeit, Ökologie, Recycling, Umwelt, Nachhaltigkeitskonzepts, Energieeffizienz

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen

## Lehrmittel

Repository




